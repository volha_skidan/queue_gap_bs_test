"""Template for programming assignment: minimum knight moves."""
from typing import List

def queue(x, y, grid):
    n = 8
    flag = [[0 for i in range(n)] for j in range(n)]
    dest = [[10000 for i in range(n)] for j in range(n)]
    que = [[x, y]]
    pointer = 0
    flag[x][y] = 1
    dest[x][y] = 0
    steps = [[1, 2], [1, -2], [-1, 2], [-1, -2], [2, -1], [2, 1], [-2, 1], [-2, -1]]
    while pointer < len(que):
        x, y = que[pointer]
        pointer += 1
        for step in steps:
            xx = x + step[0]
            yy = y + step[1]
            if (xx >= 0) and (yy >= 0) and (xx < n) and (yy < n) and (flag[xx][yy] == 0) and \
                    (grid[xx][yy] != 'O'):
                que.append([xx, yy])
                flag[xx][yy] = 1
                dest[xx][yy] = dest[x][y] + 1
                if grid[xx][yy] == 'D':
                    print(que)
                    return dest[xx][yy]


def get_minimum_knight_moves(chessboard: List[List[str]]) -> int:
    """Returns the minimum knight moves to reach the destination point."""
    n = 8

    s = [[str(e) for e in row] for row in chessboard]
    lens = [max(map(len, col)) for col in zip(*s)]
    fmt = '\t'.join('{{:{}}}'.format(x) for x in lens)
    table = [fmt.format(*row) for row in s]
    print('\n'.join(table))

    for i in range(n):
        for j in range(n):
            if chessboard[i][j] == 'K':
                return queue(i, j, chessboard)
