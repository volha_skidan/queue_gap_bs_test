"""Template for programming assignment: number of islands."""
from typing import List


def queue(x, y, flag, grid, n, m):
    print("Heeeeeeeereeeee")
    que = [[x, y]]
    pointer = 0
    flag[x][y] = 1
    steps = [[0, 1], [1, 0], [-1, 0], [0, -1]]
    while pointer < len(que):
        x, y = que[pointer]
        pointer += 1
        for step in steps:
            xx = x + step[0]
            yy = y + step[1]
            if (xx >= 0) and (yy >= 0) and (xx < n) and (yy < m) and (grid[xx][yy] == '1') and (flag[xx][yy] == 0):
                que.append([xx, yy])
                flag[xx][yy] = 1


def get_islands_count(grid: List[List[str]]) -> int:
    """Returns the number of islands in a given grid.

    Hint: you need to go over the grid and 'explore' islands
    one by one, some sort of BFS (using queue) will do.
    """

    n, m = len(grid), len(grid[0])
    flag = [[0 for i in range(m)] for j in range(n)]
    num_island = 0
    for i in range(n):
        for j in range(m):
            if (flag[i][j] == 0) and (grid[i][j] == '1'):
                num_island += 1
                queue(i, j, flag, grid, n, m)
    return num_island